# Electric charged mask
![](/assemabled.jpg)  
It is a battary powered mask using electric field as filter.
Ideally It would be reuseable and almost no commsumable part.   
The capther rate is 56.5% of 0.075 micron of NaCl.	
Pressure drop of breath is 160.72 Pa = 16.4 mmH2O.	
	
This design does not have good performance, and there are few ways to make it batter.	
1. 3D-printed sapcer can lower the resistance, however, contribution to proformance of non-woven cloth is not clear here.
2. Thicker or more layer of metal web (as electrode) to have longer capture zone in the path of inflow air. 

Rise voltage is not a good idea because it likely generate a harmful gas that damage both lung and mask, though it also harmful to virus. Also, A corona generation consume more power.

## Reasons to build a Electric charged mask

Commen medical mask and N95 mask is non-reuseable to lower the risk of cross infection.
But that require supplyment which not allways ready.
So I tried to make a filter that can be clean and reuse.

## How It works
There are 3 layers fo metal web, a non-woven cloth between each of them as a spacer.
A transformer powerd by battary suply ~kV to charging the middle layer to building electronic field between the 1st and 3rd metal web. So dust will be catpture by all three layer.	
The voltage should be lower than 2kv if you not want corona effect to generate ozone.	

I use electric fly swatter as the power sourse and material of metal web.

### BOM

1 *electric fly swatter     
2 *non-woven cloth      
1 *old mask     
n *wire 


## Build

I choose a old musk body and build a filter on It.      
2 parts are 3D printed to be filter case.   

![](/printed.jpg)

Soildering 3 metal webs with wire.      

![](/cut_swatter.jpg)  
![](/wire.jpg)  

Put material in side in the order: cover- metal web (com)- spacer- metal web (+1kV)- spacer- metal web (com).   

![](/1st.jpg)  
![](/2ed.jpg)  
![](/3rd.jpg)  

Glue each metal web with the wall of filter case as seal.   

![](/filter.jpg)  

Change the button of electric fly swatter to switch.    
Assemble all components and wire a electronics.

![](/assemabled.jpg)  

## Analyse

Make a jig for Analyse.      
![](/jig_for_analyse.jpg)  
Analyse reasult.     
![](/analyse.jpg)  